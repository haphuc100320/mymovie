import React from 'react'
import UserNav from './UserNav'

export default function HeaderTheme() {
  
  return (
    <div className='h-20 px-10 flex item-center justify-between'>
    <div style={{color:'#023047'}} className='logo text-2xl font-medium'>
    logo
      </div>
        <UserNav/>
    </div>
  )
}
