import React from 'react'
import {useSelector} from'react-redux'
import {DotLoader} from 'react-spinners'
export default function SpinnerComponent() {
  let{isLoading}=useSelector((state) => { 
    return state.spinnerReducer
   })
  return isLoading ?(
    <div 
    className="h-screen w-screen  fixed top-0 left-0 overflow-hidden flex justify-center items-center z-50">
        <DotLoader size={100}/>
    </div>
  ):('')
}
