import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { loginAction } from '../Page/Redux/Action/useAction';
import { localStorageSer } from '../service/localStorageService';

export default function UserNav() {
  let dispatch = useDispatch()
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor
  });


  const renderContent = () => {
    if (userInfor) {
      return (
        <div className='flex'>
          <p className='mt-3 mr-3'>{userInfor.hoTen}</p>
          <button onClick={handleLogout} className='border-red-300 text-white rounded px-5 py-3 border-2 bg-red-500'>Đăng Xuất</button>
        </div>
      )
    } else {
      return (
        <div>
          <button
            onClick={() => {
              window.location.href = '/login'
            }}
            className='border-red-300 text-white rounded px-5 py-3 border-2 bg-red-500'>Đăng Nhập</button>
          <button className='border-red-300 text-white rounded px-5 py-3 border-2 bg-red-500'>Đăng Xuất</button>
        </div>
      )
    }
  }
  const handleLogout = () => {
    localStorageSer.user.remove();
    dispatch(loginAction(null));
  };
  return (
    <div>{renderContent()}</div>
  )
}
