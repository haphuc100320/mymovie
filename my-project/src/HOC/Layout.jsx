import React from 'react'
import HeaderTheme from '../component/HeaderTheme'

export default function Layout({Component}) {
  return (
    <div>
        <HeaderTheme/>
       
        <Component/>
       
    </div>
  )
}
