import logo from './logo.svg';
import { BrowserRouter, Router, Routes, Route, Link } from "react-router-dom"
import './App.css';
import LoginPage from './Page/LoginPage/LoginPage';
import HomePage from './Page/HomePage/HomePage';
import Layout from './HOC/Layout';
import DetailPage from './Page/DetailPage/DetailPage';
import SpinnerComponent from './component/SpinnerComponent/SpinnerComponent';
// import HeaderTheme from './component/HeaderTheme';


function App() {
  return (
    //  phân tách trang
    <div>
      <SpinnerComponent/>
      <BrowserRouter>
          <Routes>
            <Route path='/' element={<Layout Component={HomePage} />} />
            <Route path='/detail:id' element={<Layout Component={DetailPage} />} />
            <Route path='/login' element={<LoginPage />} />
          </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
