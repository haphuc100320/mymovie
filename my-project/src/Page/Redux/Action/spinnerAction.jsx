import { BAT_LOADING, TAT_LOADING } from "../Contants/spinnerContant"

export const batLoadingAction=() => { 
    return {
        type: BAT_LOADING
    }
 }
export const tatLoadingAction=() => { 
    return {
        type: TAT_LOADING
    }
 }