import { movieService } from "../../../service/movieService"
import { SET_MOVIE_LIST } from "../Contants/movieContants"

export const getMovieListActionService =() => { 
    return (dispatch) => { 
        movieService.getMovieList()
        .then((res) => { 
            console.log(res)
            dispatch({
                type:SET_MOVIE_LIST,
                payload:res.data.content,
            })
         })
        .catch((err) => { 
            console.log(err)
         })
     }    
}