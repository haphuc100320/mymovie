import { localStorageSer } from "../../../service/localStorageService";

let initiState={
    userInfor: localStorageSer.user.get()
}
export let userReducer = (state = initiState, action) => {
    switch (action.type) {
      case "LOGIN": {
        state.userInfor = action.payload;
        return { ...state };
      }
      default:
        return state;
    }
  };
  