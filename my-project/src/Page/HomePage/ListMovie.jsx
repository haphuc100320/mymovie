import { Card } from "antd";
import Meta from "antd/lib/card/Meta";
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { movieService } from '../../service/movieService'
import { getMovieListActionService } from '../Redux/Action/movieAction'
import {NavLink} from 'react-router-dom'


export default function ListMovie() {
  let dispatch = useDispatch()
  let { movieList } = useSelector((state) => {
    return state.movieReducer
  })
  console.log("movieList", movieList)

  useEffect(() => {
    // movieService
    //   .getMovieList()
    //   .then((res) => {
    //     console.log(res);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });

    dispatch(getMovieListActionService());
  }, []);


  let renderMovieList = () => {
    return movieList.filter((item) => {
      return (item.hinhAnh !== null)
    }).map((item) => {
      return ( 
        
        <Card
      className=" rounded w-full h-90 shadow-xl hover:shadow-slate-800"
        hoverable
        style={{
          width: 240,
        }}
        cover={<img alt="example" src={item.hinhAnh} />}
        >
        <Meta title={<p className="text-blue-500 pl-20 mb-3 ">{item.biDanh}</p>}  />
        <NavLink to={`detail${item.maPhim}`}>
        <button className="w-full bg-red-600 text-white rounded text-xl font-medium py-2">Mua vé</button>
        </NavLink>
      </Card>
      )
      
      
      
    })
  }
  return (
    <div className='grid grid-cols-4 gap-6 pt-4 pl-6'>{renderMovieList()}</div>
  )
}

