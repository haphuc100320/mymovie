import React, { useEffect } from 'react'
import { Tabs } from "antd";
import { movieService } from '../../service/movieService';
import { useState } from 'react';
import ItemTabMovie from './ItemTabMovie';
import SpinnerComponent from '../../component/SpinnerComponent/SpinnerComponent';
const { TabPane } = Tabs;
export default function TabMovie() {
    const [dataMovie, setDataMovie] = useState([])
    const [isLoading,setIsloading]=useState(false)
    useEffect(() => {
        setIsloading(true)
        movieService
            .getMovieByTheater()
            .then((res) => {
                setIsloading(false)
                console.log(res);
                setDataMovie(res.data.content)

            })
            .catch((err) => {
                setIsloading(false)
                console.log(err);
            });
    }, []);
    const onChange = (key) => {
        // console.log(key);
    };
    const renderContent = () => {
        return dataMovie.map((heThongRap, index) => {
            return <TabPane tab={<img className='w-20' src={heThongRap.logo}></img>} key={index}>
                <Tabs style={{height:500}} tabPosition='left' defaultActiveKey="1" onChange={onChange}>
                    {heThongRap.lstCumRap.map((cumRap) => {
                        return <TabPane tab={renderTenCumRap(cumRap)} key={cumRap.maCumRap}>
                            <div style={{height:500, overflow:'scroll'}}>                            {cumRap.danhSachPhim.map((phim) => {
                                return <ItemTabMovie phim={phim} />
                            })}
                            </div>

                        </TabPane>
                    })}

                </Tabs>
            </TabPane>
        })
    }
    const renderTenCumRap = (cumRap) => {
        return <div className='text-left w-60 '>
            <p className='text-green-700 truncate'>{cumRap.tenCumRap}</p>
            <p className='text-black truncate'>{cumRap.diaChi}</p>
            <button className='text-red-500'>[Xem Chi Tiết]</button>

        </div>
    }
    return (
        <div className='container mx-auto  py-20 pb-96'>
            {isLoading ? <SpinnerComponent/>:''}
            <Tabs className='' style={{height:500}} tabPosition='left' defaultActiveKey="1" onChange={onChange}>
                {renderContent()}
            </Tabs>
        </div>
    )
}
