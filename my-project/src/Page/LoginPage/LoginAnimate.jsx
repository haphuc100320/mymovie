import React from 'react'
import Lottie from 'lottie-react'
import bgAnimate from '../../asset/Animation.json'
export default function LoginAnimate() {
  return (
    <div>
        <Lottie animationData={bgAnimate} loop={true}/>
    </div>
  )
}
