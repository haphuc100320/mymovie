import { Button, Checkbox, Form, Input, message } from 'antd';
import React from 'react'
import { localStorageSer } from '../../service/localStorageService';
import { userService } from '../../service/userService';
import { useNavigate } from 'react-router-dom';
import LoginAnimate from './LoginAnimate';
import { loginAction } from '../Redux/Action/useAction';
import { useDispatch } from 'react-redux';
export default function LoginPage() {
    let dispatch = useDispatch()
    // Đăng nhập thành công chuyển trang
    let history = useNavigate()
    // Hàm Đăng nhập thành công hoặc thất bại
    const onFinish = (values) => {
        userService.postLogin(values)
            .then((res) => {
                message.success("Đã Đăng Nhập")
                dispatch(loginAction(res.data.content))
                localStorageSer.user.set(res.data.content)
                // Chuyển trang
                setTimeout(() => {
                    history ('/')                   
                }, 1000);
            })
            .catch((err) => {
                // khi không thành công sẽ thông báo
                message.error(err.response.data.content)

            })
    };





    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div className='bg-red-400 h-screen w-screen p-10'>
            <div className='container bg-white mx-auto rounded-xl p-10 flex w-3/4'>
                <div className='h-1/4 w-80 mr-3 overflow-hidden'>
                    <LoginAnimate/>
                </div>
                <div className='w-1/2 py-10'>
                <Form
                    name="basic"
                    layout='vertical'
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 24,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        label="Tài Khoản"
                        name="taiKhoan"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your username!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Mật Khẩu"
                        name="matKhau"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    <div className='flex justify-center'>
                        <button className='rounded px-5 py-2 text-white bg-red-500'>Đăng Nhập</button>
                    </div>
                </Form>
                </div>
            </div>
        </div>
    );
};
